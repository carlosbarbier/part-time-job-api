from dataclasses import dataclass


@dataclass
class EmployerDto:
    id: str
    name: str
    email: str


@dataclass
class CategoryDto:
    id: str
    name: str


@dataclass
class JobDto:
    title: str
    description: str
    job_type: str
    address: str
    latitude: str
    longitude: str
    city: str
    hours_per_weeks: str
