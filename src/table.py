

from botocore.exceptions import ClientError


class Table:
    def __init__(self, dyn_resource):

        self.dyn_resource = dyn_resource
        self.dynamo_table = None

    def create_table(self, table_name):

        try:
            self.dynamo_table = self.dyn_resource.create_table(
                TableName=table_name,
                KeySchema=[
                    {
                        'AttributeName': 'id',
                        'KeyType': 'HASH'
                    }
                ],
                AttributeDefinitions=[
                    {
                        'AttributeName': 'id',
                        'AttributeType': 'S'
                    }
                ],
                ProvisionedThroughput={
                    'ReadCapacityUnits': 1,
                    'WriteCapacityUnits': 1
                }


            )
        except ClientError as err:
            print(
                "Couldn't create table %s. Here's why: %s: %s", table_name,
                err.response['Error']['Code'], err.response['Error']['Message'])
            raise
        else:
            return self.dynamo_table

