from flask import Flask
from flask_cors import CORS

from src.exception import GlobalException
from src.routes import register_routes


def create_app():
    application = Flask(__name__)

    application.config['WTF_CSRF_ENABLED'] = False  # Sensitive
    # cors
    CORS(application, resources={r"/*": {"origins": "*"}})
    # route
    register_routes(application)
    # exception
    GlobalException.handle(application)

    return application
