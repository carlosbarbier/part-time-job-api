import os

import boto3

from dotenv import load_dotenv
from werkzeug.exceptions import BadRequest, InternalServerError, Unauthorized,Forbidden
load_dotenv()

INTERNAL_SERVER_ERROR = "Internal server error"
EXPIRED_TOKEN = "Invalid or expired token"

COGNITO_USER_CLIENT_ID = os.environ["COGNITO_USER_CLIENT_ID"]
AWS_ACCESS_KEY_ID = os.environ["SECRET_ACCESS_KEY"]
AWS_SECRET_ACCESS_KEY = os.environ["SECRET_ACCESS_KEY"]
REGION_NAME = os.environ["REGION_NAME"]


client = boto3.client('cognito-idp',
                      aws_access_key_id=AWS_ACCESS_KEY_ID,
                      aws_secret_access_key=AWS_SECRET_ACCESS_KEY,
                      region_name=REGION_NAME)


def register_new_user(email: str, name: str, password: str):
    try:
        response = client.sign_up(
            ClientId=COGNITO_USER_CLIENT_ID,
            Username=email,
            Password=password,
            UserAttributes=[
                {"Name": "email", "Value": email},
                {"Name": "name", "Value": name}
            ],
        )

        print(response)
        return response
    except client.exceptions.InvalidPasswordException as e:
        print(e)
        raise BadRequest("Invalid email or password")
    except client.exceptions.UsernameExistsException as e:
        print(e)
        raise BadRequest('Email taken')
    except client.exceptions.InvalidParameterException as e:
        print(e)
        raise BadRequest("Invalid email or password")
    except Exception as e:
        print(e)
        raise InternalServerError()


def confirm_user_signup(email: str, confirm_code):
    try:
        response = client.confirm_sign_up(
            ClientId=COGNITO_USER_CLIENT_ID,
            Username=email,
            ConfirmationCode=confirm_code,
        )

        print(response)

    except client.exceptions.NotAuthorizedException as e:
        print(e)
        raise Unauthorized(EXPIRED_TOKEN)
    except client.exceptions.ExpiredCodeException as e:
        print(e)
        raise BadRequest('Confirmation code expired')
    except Exception as e:
        print(e)
        raise InternalServerError(INTERNAL_SERVER_ERROR)


def authenticate_user(email: str, password: str):
    try:
        response = client.initiate_auth(
            ClientId=COGNITO_USER_CLIENT_ID,
            AuthFlow="USER_PASSWORD_AUTH",
            AuthParameters={"USERNAME": email, "PASSWORD": password},
        )
        return response
    except client.exceptions.NotAuthorizedException as e:
        print(e)
        raise BadRequest("Incorrect email or password")
    except client.exceptions.UserNotConfirmedException as e:
        print(e)
        raise Forbidden("Please validate your account")
    except Exception as e:
        print(e)
        raise InternalServerError(INTERNAL_SERVER_ERROR)


def get_user(access_token: str):
    try:
        user = client.get_user(AccessToken=access_token)
        data = {
            'id': user['Username'],
            user['UserAttributes'][2]['Name']: user['UserAttributes'][2]['Value'],
            user['UserAttributes'][3]['Name']: user['UserAttributes'][3]['Value']
        }
        return data
    except client.exceptions.NotAuthorizedException as e:
        print(e)
        raise Unauthorized("Invalid or expired token")
    except Exception as e:
        print(e)
        raise InternalServerError(INTERNAL_SERVER_ERROR)


def forgot_password(email: str):
    try:
        response = client.forgot_password(
            ClientId=COGNITO_USER_CLIENT_ID,
            Username=email
        )

        print(response)
    except client.exceptions.NotAuthorizedException as e:
        print(e)
        raise Unauthorized(EXPIRED_TOKEN)
    except Exception as e:
        print(e)
        raise InternalServerError(INTERNAL_SERVER_ERROR)


def reset_password(email: str, password: str, confirm_code: str):
    try:
        response = client.confirm_forgot_password(
            ClientId=COGNITO_USER_CLIENT_ID,
            Username=email,
            ConfirmationCode=confirm_code,
            Password=password
        )

        print(response)

    except client.exceptions.NotAuthorizedException as e:
        raise Unauthorized(EXPIRED_TOKEN)
    except client.exceptions.ExpiredCodeException as e:
        print(e)
        raise BadRequest('Confirmation code expired')
    except Exception as e:
        print(e)
        raise InternalServerError(INTERNAL_SERVER_ERROR)


def change_password(previous_password: str, new_password: str, access_token: str):
    try:
        response = client.change_password(
            PreviousPassword=previous_password,
            ProposedPassword=new_password,
            AccessToken=access_token
        )

        print(response)
    except Exception as e:
        print(e)
        raise InternalServerError(INTERNAL_SERVER_ERROR)


def resend_confirmation_code(email: str):
    try:
        response = client.resend_confirmation_code(
            ClientId=COGNITO_USER_CLIENT_ID,
            Username=email,
        )

        print(response)
    except client.exceptions.NotAuthorizedException as e:
        print(e)
        raise Unauthorized(EXPIRED_TOKEN)
    except Exception as e:
        print(e)
        raise InternalServerError(INTERNAL_SERVER_ERROR)


def logout(access_token: str):
    try:
        response = client.global_sign_out(
            AccessToken=access_token
        )
        print(response)
    except client.exceptions.NotAuthorizedException as e:
        print(e)
        raise Unauthorized(EXPIRED_TOKEN)
    except Exception as e:
        print(e)
        raise InternalServerError(INTERNAL_SERVER_ERROR)


def delete_user_account(access_token: str):
    try:
        response = client.delete_user(
            AccessToken=access_token
        )
        print(response)
    except client.exceptions.NotAuthorizedException as e:
        print(e)
        raise Unauthorized(EXPIRED_TOKEN)
    except Exception as e:
        print(e)
        raise InternalServerError(INTERNAL_SERVER_ERROR)
