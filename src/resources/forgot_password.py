from flask_restful import reqparse, Resource

from src import service
from src.validation import email_validator


class ForgotPasswordResource(Resource):
    def __init__(self):
        self.parser = reqparse.RequestParser(bundle_errors=True)

    def post(self):
        email_validator(self.parser)
        args = self.parser.parse_args()
        service.forgot_password(args)
        return {'message': 'verify your email for verification code'}
