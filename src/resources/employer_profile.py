import http

from flask import request
from flask_restful import Resource, reqparse

from src import service
from src.util import employer_auth
from src.validation import profile_validator


class EmployerProfileResource(Resource):
    def __init__(self):
        self.parser = reqparse.RequestParser(bundle_errors=True)

    def put(self):
        profile_validator(self.parser)
        args = self.parser.parse_args()
        user = employer_auth(request)
        service.update_profile(args=args, employer_id=user['id'])
        return {}, http.HTTPStatus.OK


class CategoryItemResource(Resource):
    def get(self, category_id: str):
        return service.get_category(category_id)
