import http

from flask import request
from flask_restful import Resource, reqparse

from src import service
from src.util import employer_auth, user_auth
from src.validation import job_creation_validation


class JobResource(Resource):
    def __init__(self):
        self.parser = reqparse.RequestParser(bundle_errors=True)

    def get(self):
        return service.get_all_jobs()

    def post(self):
        job_creation_validation(self.parser)
        args = self.parser.parse_args()
        employer = employer_auth(request=request)
        print(employer)
        employer_data = {'id': employer['id'], 'role': employer['role'], 'image': employer['image'],
                         'name': employer['name']}
        service.create_new_job(args, employer=employer_data)
        return {}


class ApplyJobResource(Resource):
    def __init__(self):
        self.parser = reqparse.RequestParser(bundle_errors=True)

    def post(self, job_id: str):
        user = user_auth(request=request)
        user_data = {'id': user['id'], 'role': user['role'], 'email': user['email'], 'name': user['name']}
        service.job_apply(job_id=job_id, user_data=user_data)
        return {}, http.HTTPStatus.OK


class JobItemResource(Resource):
    def __init__(self):
        self.parser = reqparse.RequestParser(bundle_errors=True)

    def get(self, job_id: str):
        return service.get_job(job_id=job_id)


class JobApplicationsResource(Resource):
    def get(self):
        user = user_auth(request=request)
        return service.get_my_applications(user['id'])


class EmployerJobResource(Resource):
    def get(self):
        employer = employer_auth(request=request)
        return service.get_employer_jobs_created(employer['id'])
