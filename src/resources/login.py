from flask_restful import Resource, reqparse

from src import service
from src.validation import user_login_validator


class LoginUserResource(Resource):
    def __init__(self):
        self.parser = reqparse.RequestParser(bundle_errors=True)

    def post(self):
        user_login_validator(self.parser)
        args = self.parser.parse_args(http_error_code=422)
        user = service.login(args)
        return user
