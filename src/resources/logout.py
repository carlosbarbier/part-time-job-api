from flask import request
from flask_restful import Resource

from src import service
from src.util import auth, get_access_token


class LogoutResource(Resource):
    @auth()
    def post(self):
        access_token = get_access_token(request=request)
        service.logout(access_token)
        return {}
