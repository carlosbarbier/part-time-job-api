from flask import request
from flask_restful import Resource

from src import service
from src.util import get_access_token


class DeleteUserResource(Resource):
    def post(self):
        access_token = get_access_token(request=request)
        return service.delete_user(access_token)
