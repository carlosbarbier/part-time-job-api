from flask import request
from flask_restful import Resource, reqparse

from src import service
from src.util import auth, get_access_token
from src.validation import user_change_password_validator


class ChangePasswordResource(Resource):
    def __init__(self):
        self.parser = reqparse.RequestParser(bundle_errors=True)

    @auth()
    def post(self):
        user_change_password_validator(self.parser)
        args = self.parser.parse_args()
        access_token = get_access_token(request)
        service.change_password(args=args, access_token=access_token)
        return {},
