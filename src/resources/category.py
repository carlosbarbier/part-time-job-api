from flask_restful import Resource

from src import service


class CategoryResource(Resource):

    def get(self):
        return service.get_categories()


class CategoryItemResource(Resource):
    def get(self, category_id: str):
        return service.get_category(category_id)
