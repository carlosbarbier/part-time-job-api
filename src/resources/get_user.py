from flask import request
from flask_restful import Resource

from src import service
from src.util import get_access_token


class GetUserResource(Resource):
    def get(self):
        access_token = get_access_token(request=request)
        return service.get_user_details(access_token)
