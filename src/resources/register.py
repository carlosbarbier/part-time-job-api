from flask_restful import Resource, reqparse

from src import service
from src.validation import user_registration_validator


class RegisterUserResource(Resource):
    def __init__(self):
        self.parser = reqparse.RequestParser(bundle_errors=True)

    def post(self):
        user_registration_validator(self.parser)
        args = self.parser.parse_args(http_error_code=422)
        service.register(args,False)
        return {"message": "verify your email for the verification code"}, 201
