from src import cognito, dynamodb
from src.dto import JobDto
from src.notfication import send_notification
from src.util import image_url_builder, image_key_builder, move_image_to_s3


def register(args, is_employer: bool):
    name = args['name']
    email = args['email']
    password = args['password']
    response = cognito.register_new_user(name=name, email=email, password=password)
    if response['UserSub'] is not None:
        dynamo_response = dynamodb.add_user(response['UserSub'], name=name, email=email, is_employer=is_employer)
        print(dynamo_response)


def login(args):
    email = args['email']
    password = args['password']
    response = cognito.authenticate_user(email=email, password=password)
    access_token = response["AuthenticationResult"]["AccessToken"]
    expires_in = response["AuthenticationResult"]["ExpiresIn"]
    refresh_token = response["AuthenticationResult"]["RefreshToken"]
    user = cognito.get_user(access_token)
    db_found = dynamodb.get_user(user['id'])
    data = {"access_token": access_token, 'expires_in': expires_in, "refresh_token": refresh_token,
            'role': db_found['role']}
    return data


def get_user(user_id: str):
    return dynamodb.get_user(user_id=user_id)


def get_user_details(access_token: str):
    return cognito.get_user(access_token=access_token)


def change_password(args, access_token: str):
    new_password = args['new_password']
    previous_password = args['previous_password']
    cognito.change_password(previous_password=previous_password, new_password=new_password, access_token=access_token)


def update_profile(args, employer_id: str):
    phone = args['phone'][0] if type(args['phone']) == list else args['phone']
    address = args['address'][0] if type(args['address']) == list else args['address']
    image = args['image']

    move_image_to_s3(image)
    url = image_url_builder(image.filename)
    key = image_key_builder(image.filename)

    data = {
        'image': url,
        'phone': phone,
        'address': address,
        'image_key': key
    }
    dynamodb.update_profile(user_id=employer_id, data=data)


def forgot_password(args):
    email = args['email']
    cognito.forgot_password(email)


def logout(access_token: str):
    cognito.logout(access_token=access_token)


def reset_password(args):
    email = args['email']
    confirm_code = args['confirm_code']
    password = args['password']
    cognito.reset_password(email=email, password=password, confirm_code=confirm_code)


def confirm_email(args):
    email = args['email']
    confirm_code = args['confirm_code']
    cognito.confirm_user_signup(email=email, confirm_code=confirm_code)


def resend_code(args):
    email = args['email']
    cognito.resend_confirmation_code(email=email)


def delete_user(access_token):
    cognito.delete_user_account(access_token)


def get_categories():
    return dynamodb.get_categories()


def get_category(category_id: str):
    return dynamodb.get_category(category_id=category_id)


def get_job(job_id: str):
    return dynamodb.get_job(job_id=job_id)


def create_new_job(args, employer: dict):
    title = args['title']
    description = args['description']
    job_type = args['job_type']
    address = args['address']
    latitude = args['latitude']
    longitude = args['longitude']
    city = args['city']
    hours_per_weeks = args['hours_per_weeks']
    category_id = args['category_id']

    category = get_category(category_id=category_id)
    dto = JobDto(title=title, description=description, address=address, longitude=longitude, city=city,
                 hours_per_weeks=hours_per_weeks, latitude=latitude, job_type=job_type)
    dynamodb.post_job(dto=dto, employer=employer, category=category)


def job_apply(job_id: str, user_data: dict):
    job = get_job(job_id)
    print(job)
    job_name = job['title']

    employer_data = get_user(job['employer']['id'])

    user_email = user_data['email']
    employer_email = employer_data['email']
    user_name = user_data['name']

    args = {
        'user_email': user_email,
        'employer_email': employer_email,
        'user_name': user_name,
        'job': job_name
    }
    dto = JobDto(title=job['title'], description=job['description'], address=job['address'], longitude=job['longitude'],
                 city=job['city'],
                 hours_per_weeks=job['hours_per_weeks'], latitude=job['latitude'], job_type=job['type'])
    dynamodb.my_job_application(dto=dto, employer=job['employer'], user_id=user_data['id'])
    send_notification(args)


def get_all_jobs():
    return dynamodb.get_all_jobs()


def get_my_applications(user_id: str):
    return dynamodb.get_my_applications(user_id)


def get_employer_jobs_created(user_id: str):
    return dynamodb.get_employers_job(user_id)
