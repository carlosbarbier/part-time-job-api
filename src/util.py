import os
from datetime import datetime
from functools import wraps

import flask_restful
from flask import request
from werkzeug.exceptions import Unauthorized, BadRequest, InternalServerError
from werkzeug.utils import secure_filename

from src import cognito, dynamodb
from src.file import upload_file

NOT_AUTHORIZED = 'not authorized'

BEARER = 'Bearer'

ALLOWED_IMAGE_EXTENSIONS = ['jpg', 'jpeg', 'png']


def non_empty_string(s):
    if not s:
        raise ValueError("Must not be empty string")
    return s


def auth():
    def _auth(f):
        @wraps(f)
        def __auth(*args, **kwargs):
            if 'Authorization' in request.headers:
                token = request.headers.get('Authorization')
                if token is not None and len(token) > 10:
                    access_token = token.replace('%s ' % BEARER, '')
                    user = cognito.get_user(access_token=access_token)
                    if user is not None:
                        return f(*args, **kwargs)
                    flask_restful.abort(http_status_code=401, message=NOT_AUTHORIZED)
                else:
                    flask_restful.abort(http_status_code=401, message=NOT_AUTHORIZED)
            else:
                flask_restful.abort(http_status_code=400, message="Invalid or expired token")

        return __auth

    return _auth


def get_access_token(request):
    token = request.headers.get('Authorization')
    if token is not None and len(token) > 10:
        return token.replace('Bearer ', '')
    raise Unauthorized('Invalid or expired token')


def employer_auth(request):
    token = request.headers.get('Authorization')
    if token is not None and len(token) > 10:
        access_token = token.replace('%s ' % BEARER, '')
        user = cognito.get_user(access_token=access_token)
        if user is not None:
            user = dynamodb.get_user(user_id=user['id'])
            print(user)
            if user['role'] != 'employer':
                raise Unauthorized(NOT_AUTHORIZED)
            return {
                'id': user['id'],
                'access_token': access_token,
                'role': user['role'],
                'image': user['image'],
                'email': user['email'],
                'name': user['name']
            }
        raise Unauthorized(NOT_AUTHORIZED)
    else:
        raise Unauthorized(NOT_AUTHORIZED)


def user_auth(request):
    token = request.headers.get('Authorization')
    if token is not None and len(token) > 10:
        access_token = token.replace('%s ' % BEARER, '')
        user = cognito.get_user(access_token=access_token)
        if user is not None:
            user = dynamodb.get_user(user_id=user['id'])

            if user['role'] != 'user':
                raise Unauthorized(NOT_AUTHORIZED)
            return {
                'id': user['id'],
                'access_token': access_token,
                'role': user['role'],
                'image': user['image'],
                'email': user['email'],
                'name': user['name']
            }
        else:
            raise Unauthorized(NOT_AUTHORIZED)
    else:
        raise Unauthorized(NOT_AUTHORIZED)


def validate_image_extension(file):
    return '.' in file.filename and file.filename.rsplit('.', 1)[1].lower() in ALLOWED_IMAGE_EXTENSIONS


def image_url_builder(filename):
    return 'https://part-time-api-users-bk.s3.amazonaws.com/assets/images/' + filename


def image_key_builder(filename):
    return 'assets/images/' + filename


def move_image_to_s3(image):
    folder = "assets/images"
    if not validate_image_extension(image):
        raise BadRequest('Invalid image format')
    image.filename = current_time_ms() + '-' + image.filename
    try:
        image.save(os.path.join(folder, secure_filename(image.filename)))
        response = upload_file(f"assets/images/{image.filename}")
        if response is None:
            os.remove(os.path.join(folder, image.filename))
    except Exception as e:
        print(e)
        raise InternalServerError('an error occurred')


def current_time_ms():
    dt = datetime.now()
    return str(round(dt.timestamp() * 1000))
