from flask_restful import inputs
from werkzeug.datastructures import FileStorage

from src.util import non_empty_string

PASSWORD_REQUIRED = 'valid password required'

EMAIL_REGEX = r"(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)"

EMAIL_REQUIRED = 'valid email required'


def user_registration_validator(parser):
    parser.add_argument('name', help='valid name required', required=True, nullable=False, type=non_empty_string,
                        trim=True)
    parser.add_argument('email', help=EMAIL_REQUIRED,
                        nullable=False)
    parser.add_argument('password', help=PASSWORD_REQUIRED, required=True, nullable=False,
                        type=non_empty_string)


def employer_validator(parser):
    parser.add_argument('name', help='company name required', required=True, nullable=False, type=non_empty_string,
                        trim=True)
    parser.add_argument('email', help=EMAIL_REQUIRED,
                        nullable=False)
    parser.add_argument('password', help=PASSWORD_REQUIRED, required=True, nullable=False,
                        type=non_empty_string)


def profile_validator(parser):
    parser.add_argument('phone', help='phone number required', required=True, nullable=False,
                        action='append')
    parser.add_argument('address', help='address required', required=True, nullable=False,
                        action='append')
    parser.add_argument('image', type=FileStorage, location='files', help='profile image required', required=True,
                        nullable=False)


def user_login_validator(parser):
    parser.add_argument('email', help=EMAIL_REQUIRED,
                        type=inputs.regex(EMAIL_REGEX), nullable=False)
    parser.add_argument('password', help=PASSWORD_REQUIRED, required=True, nullable=False,
                        type=non_empty_string)


def email_validator(parser):
    parser.add_argument('email', help=EMAIL_REQUIRED,
                        type=inputs.regex(EMAIL_REGEX), nullable=False)


def user_reset_password_validator(parser):
    parser.add_argument('email', help=EMAIL_REQUIRED,
                        type=inputs.regex(EMAIL_REGEX), nullable=False)
    parser.add_argument('password', help=PASSWORD_REQUIRED, required=True, nullable=False,
                        type=non_empty_string)
    parser.add_argument('confirm_code', type=str, help='confirm_code required', required=True, nullable=False)


def user_change_password_validator(parser):
    parser.add_argument('previous_password', help=PASSWORD_REQUIRED, required=True, nullable=False,
                        type=non_empty_string)
    parser.add_argument('new_password', type=str, help='Current password required',
                        required=True, nullable=False)
    parser.add_argument('password_confirmation', type=str, help='Password confirmation required',
                        required=True, nullable=False)


def user_email_confirmation_validator(parser):
    parser.add_argument('email', help=EMAIL_REQUIRED,
                        type=inputs.regex(EMAIL_REGEX), nullable=False)
    parser.add_argument('confirm_code', type=str, help='token required', required=True, nullable=False)


def job_creation_validation(parser):
    parser.add_argument('title', type=str, help='title required', required=True, nullable=False)
    parser.add_argument('description', type=str, help='description required', required=True, nullable=False)
    parser.add_argument('job_type', type=str, help='job type required', required=True, nullable=False)
    parser.add_argument('address', type=str, help='address required', required=True, nullable=False)
    parser.add_argument('latitude', type=str, help='latitude required', required=True, nullable=False)
    parser.add_argument('longitude', type=str, help='longitude required', required=True, nullable=False)
    parser.add_argument('city', type=str, help='city required', required=True, nullable=False)
    parser.add_argument('hours_per_weeks', type=str, help='hours per weeks required', required=True, nullable=False)
    parser.add_argument('category_id', type=str, help='category id required', required=True, nullable=False)
