import datetime
import os
import uuid

from boto3 import resource
from boto3.dynamodb.conditions import Key, Attr
from werkzeug.exceptions import InternalServerError, NotFound

from src.dto import JobDto

AWS_ACCESS_KEY_ID = os.environ["ACCESS_KEY_ID"]
AWS_SECRET_ACCESS_KEY = os.environ["SECRET_ACCESS_KEY"]
REGION_NAME = os.environ["REGION_NAME"]
INTERNAL_SERVER_ERROR = "Internal server error"
resource = resource(
    'dynamodb',
    aws_access_key_id=AWS_ACCESS_KEY_ID,
    aws_secret_access_key=AWS_SECRET_ACCESS_KEY,
    region_name=REGION_NAME,
)

USER_TABLE = resource.Table('USERS')
CATEGORIES_TABLE = resource.Table('CATEGORIES')
JOBS_TABLE = resource.Table('JOBS')
APPLICATIONS_TABLE = resource.Table('APPLICATIONS')


def add_user(user_id, name, email, is_employer: bool):
    item: dict
    try:
        if is_employer:
            item = {
                'id': user_id,
                'name': name,
                'email': email,
                'role': 'employer',
                'image': 'https://part-time-api-users-bk.s3.amazonaws.com/images/no-image.png',
                'phone': 'none',
                'address': 'none',
                'image_key': 'none'

            }
        else:
            item = {
                'id': user_id,
                'name': name,
                'email': email,
                'image': 'https://part-time-api-users-bk.s3.amazonaws.com/images/no-image.png',
                'role': 'user'
            }
        response = USER_TABLE.put_item(Item=item)

        return response
    except Exception as e:
        print(e)
        raise InternalServerError(INTERNAL_SERVER_ERROR)


def update_profile(user_id: str, data: dict):
    try:
        response = USER_TABLE.update_item(
            Key={
                'id': user_id
            },
            AttributeUpdates={
                'image': {
                    'Value': data['image'],
                    'Action': 'PUT'
                },
                'phone': {
                    'Value': data['phone'],
                    'Action': 'PUT'
                },
                'image_key': {
                    'Value': data['image_key'],
                    'Action': 'PUT'
                },
                'address': {
                    'Value': data['address'],
                    'Action': 'PUT'
                }
            },
            ReturnValues="UPDATED_NEW"
        )
        return response
    except Exception as e:
        print(e)
        raise InternalServerError(INTERNAL_SERVER_ERROR)


def get_user(user_id: str):
    try:
        result = None
        response = USER_TABLE.get_item(
            Key={'id': user_id},
            AttributesToGet=['id', 'name', 'email', 'image', 'role']
        )
        if 'Item' in response:
            result = response['Item']

    except Exception as e:
        print(e)
        raise InternalServerError(INTERNAL_SERVER_ERROR)

    if result is None:
        raise NotFound('user not found')
    return result


def update_user(user_id: str, data: dict):
    try:

        response = USER_TABLE.update_item(
            Key={
                'id': user_id
            },
            AttributeUpdates={
                'name': {
                    'Value': data['name'],
                    'Action': 'PUT'
                },
                'email': {
                    'Value': data['email'],
                    'Action': 'PUT'
                }
            },
            ReturnValues="UPDATED_NEW"
        )
        return response
    except Exception as e:
        print(e)
        raise InternalServerError(INTERNAL_SERVER_ERROR)


def delete_user(id: str):
    try:
        return USER_TABLE.delete_item(
            Key={
                'id': id
            }
        )
    except Exception as e:
        print(e)
        raise InternalServerError(INTERNAL_SERVER_ERROR)


def get_categories():
    try:
        response = CATEGORIES_TABLE.scan()
        data = response['Items']
        while 'LastEvaluatedKey' in response:
            response = CATEGORIES_TABLE.scan(ExclusiveStartKey=response['LastEvaluatedKey'])
            data.extend(response['Items'])
        return data
    except Exception as e:
        print(e)
        raise InternalServerError(INTERNAL_SERVER_ERROR)


def get_category(category_id: str):
    try:
        result = None
        response = CATEGORIES_TABLE.get_item(
            Key={'id': category_id},
            AttributesToGet=['id', 'name']
        )
        if 'Item' in response:
            result = response['Item']

    except Exception as e:
        print(e)
        raise InternalServerError(INTERNAL_SERVER_ERROR)

    if result is None:
        raise NotFound('Category not found')
    return result


def post_job(dto: JobDto, employer: dict, category: dict):
    try:
        JOBS_TABLE.put_item(
            Item={
                'id': uuid.uuid4().hex,
                'title': dto.title,
                'description': dto.description,
                'type': dto.job_type,
                'created_at': str(datetime.datetime.now()),
                'update_at': str(datetime.datetime.now()),
                'address': dto.address,
                'latitude': dto.latitude,
                'longitude': dto.longitude,
                'city': dto.city,
                'hours_per_weeks': dto.hours_per_weeks,
                'category': category,
                'employer': employer

            })
    except Exception as e:
        print(e)
        raise InternalServerError(INTERNAL_SERVER_ERROR)


def get_all_jobs():
    try:
        response = JOBS_TABLE.scan()
        data = response['Items']
        while 'LastEvaluatedKey' in response:
            response = JOBS_TABLE.scan(ExclusiveStartKey=response['LastEvaluatedKey'])
            data.extend(response['Items'])
        return data
    except Exception as e:
        print(e)
        raise InternalServerError(INTERNAL_SERVER_ERROR)


def get_job(job_id: str):
    try:
        result = None
        response = JOBS_TABLE.get_item(
            Key={'id': job_id},
        )
        if 'Item' in response:
            result = response['Item']

    except Exception as e:
        print(e)
        raise InternalServerError(INTERNAL_SERVER_ERROR)

    if result is None:
        raise NotFound('job not found')
    return result


def my_job_application(dto: JobDto, employer: dict, user_id: str):
    try:
        APPLICATIONS_TABLE.put_item(
            Item={
                'id': uuid.uuid4().hex,
                'user_id': user_id,
                'title': dto.title,
                'description': dto.description,
                'type': dto.job_type,
                'address': dto.address,
                'latitude': dto.latitude,
                'longitude': dto.longitude,
                'city': dto.city,
                'hours_per_weeks': dto.hours_per_weeks,
                'employer': employer,
                'apply_date': str(datetime.datetime.now())

            })
    except Exception as e:
        print(e)
        raise InternalServerError(INTERNAL_SERVER_ERROR)


def get_my_applications(user_id: str):
    try:
        response = APPLICATIONS_TABLE.scan(FilterExpression=Attr('user_id').eq(user_id))
        data = response['Items']
        while 'LastEvaluatedKey' in response:
            response = APPLICATIONS_TABLE.scan(ExclusiveStartKey=response['LastEvaluatedKey'])
            data.extend(response['Items'])
        return data
    except Exception as e:
        print(e)
        raise InternalServerError(INTERNAL_SERVER_ERROR)


def get_employers_job(user_id: str):
    try:
        response = JOBS_TABLE.scan(FilterExpression=Attr('employer.id').eq(user_id))
        data = response['Items']
        while 'LastEvaluatedKey' in response:
            response = JOBS_TABLE.scan(ExclusiveStartKey=response['LastEvaluatedKey'])
            data.extend(response['Items'])
        return data
    except Exception as e:
        print(e)
        raise InternalServerError(INTERNAL_SERVER_ERROR)
