import os

import requests
from dotenv import load_dotenv

load_dotenv()

MAILGUN_PRIVATE_KEY = os.environ["MAILGUN_PRIVATE_KEY"]
MAILGUN_DOMAIN = os.environ["MAILGUN_DOMAIN"]


def send_notification(args):
    user_email = args['user_email']
    employer_email = args['employer_email']
    user = args['user_name']
    job = args['job']

    try:
        send_message(user_email=user_email, employer_email=employer_email, user=user,
                     job=job)
    except Exception as e:
        print(e)


def send_message(user: str, job: str, user_email: str, employer_email: str):
    requests.post(
        "https://api.mailgun.net/v3/" + MAILGUN_DOMAIN + "/messages",
        auth=("api", MAILGUN_PRIVATE_KEY),
        data={"from": "partime@app.com",
              "to": [employer_email],
              "subject": "Job Application",
              "html": email_template(user=user, job=job, user_email=user_email)
              })


def email_template(user, job, user_email):
    return f""" 
            <!doctype html>
            <html>
              <head>
                <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
              </head>
              <body>
                <p>Hi ,</p>
                <p>{user} has applied for {job} job.</p>
                Here his email:{user_email} in case you want to contact him.</p>
                <p>Thanks !</p>
                <p>Partime job Team.</p>
              </body>
            </html>
            """
