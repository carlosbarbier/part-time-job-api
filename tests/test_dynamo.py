import unittest

import pytest
from botocore import stub

from src.dynamodb import USER_TABLE, get_user, add_user


@pytest.fixture(scope="function")
def dynamo_stubber():
    ddb_stubber = stub.Stubber(USER_TABLE.meta.client)
    ddb_stubber.activate()
    yield ddb_stubber
    ddb_stubber.deactivate()


def test_user_exists(dynamo_stubber):
    user_id = 'user123'
    get_item_params = {'AttributesToGet': ['id', 'name', 'email'],
                       'Key': {'id': user_id},
                       'TableName': 'USERS'}

    response = {'Item': {'id': {'S': user_id},
                         'name': {'S': 'Spam'},
                         'email': {'S': 'kouakou@gmail.com'}
                         }}
    dynamo_stubber.add_response('get_item', response, get_item_params)

    result = get_user(user_id)

    assert result['Item']['id'] == user_id
    dynamo_stubber.assert_no_pending_responses()


def test_user_missing(dynamo_stubber):
    user_id = 'user123'
    get_item_params = {'AttributesToGet': ['id', 'name', 'email'],
                       'Key': {'id': user_id},
                       'TableName': 'USERS'}
    response = {}
    dynamo_stubber.add_response('get_item', response, get_item_params)

    result = get_user(user_id)

    assert result == response
    dynamo_stubber.assert_no_pending_responses()


def test_save(dynamo_stubber):
    put_item = {'Item': {'email': 'kouakou@gmail.com', 'id': '12345', 'name': 'carlos'}, 'TableName': 'USERS'}
    response = {
        "ResponseMetadata": {
            "HTTPStatusCode": 200
        }
    }
    dynamo_stubber.add_response('put_item', response, put_item)
    result = add_user("12345", "carlos", "kouakou@gmail.com",False)
    dynamo_stubber.assert_no_pending_responses()

    assert 200 == result['ResponseMetadata']['HTTPStatusCode']


if __name__ == '__main__':
    unittest.main()
